import QtQuick          2.3
import QtQuick.Dialogs  1.2
import QtQuick.Layouts  1.2
import QtQuick.Window   2.11

import QGroundControl.Controls      1.0
import QGroundControl.ScreenTools   1.0
import QGroundControl.FactSystem    1.0
import QGroundControl.FactControls  1.0
import QGroundControl.Controllers   1.0
import QGroundControl.Palette       1.0
import QGroundControl               1.0

import CustomAltitudeAdding         1.0

Item {
    id: customTopLeftValuesWidget

    property var    _activeVehicle: QGroundControl.multiVehicleManager.activeVehicle ? QGroundControl.multiVehicleManager.activeVehicle : QGroundControl.multiVehicleManager.offlineEditingVehicle
    property var    _heading: _activeVehicle ? _activeVehicle.heading.rawValue : 0



        Row{
            spacing: 50
//            Loader {
//                sourceComponent: element
//                property bool isFact: false
//                property var txt: "Pusula"
//                property var value: _heading
//                property var compColor: "#7db1ff"

//            }
            Loader {
                sourceComponent: element
                property bool isFact: true
                property var txt: "Derinlik"
                property var value: _activeVehicle.getFact("altitudeRelative")
                property var compColor: "white"
            }
            Loader {
                sourceComponent: element
                property bool isFact: true
                property var txt: "Güç"
                property var value: _activeVehicle.getFact("APMSubInfo.pilot gain")
                property var compColor: "red"
            }
        }





    Component{
        id:element

        Column {

            QGCLabel {
                horizontalAlignment:    Text.AlignHCenter
                wrapMode:               Text.WordWrap
                text:                   txt
                color:                  compColor !== undefined ? compColor : qgcPal.text
                font.bold:              true
                font.pointSize:         ScreenTools.mediumFontPointSize * 2

            }
            QGCLabel {
                horizontalAlignment:    Text.AlignHCenter
                font.pointSize:         ScreenTools.largeFontPointSize * 2
                font.family:            ScreenTools.demiboldFontFamily
                fontSizeMode:           Text.HorizontalFit
                text:                   isFact ? (txt === "Güç" ? "%" + value.enumOrValueString : (Math.round((value.value - CustomAltitudeAdding.altitudeAddingValue) * 10) / 10).toString() + "m"): value
                color:                  compColor !== undefined ? compColor : qgcPal.text
                font.bold:              true
            }
        }
    }
}
