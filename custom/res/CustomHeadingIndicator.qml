
import QtQuick                  2.11
import QtQuick.Controls         2.4
import QtQuick.Layouts          1.11
import QtQuick.Dialogs          1.3
import QtPositioning            5.2
import QtQuick.Window           2.11

import QGroundControl                       1.0
import QGroundControl.Controllers           1.0
import QGroundControl.Controls              1.0
import QGroundControl.FlightMap             1.0
import QGroundControl.MultiVehicleManager   1.0
import QGroundControl.Palette               1.0
import QGroundControl.ScreenTools           1.0
import QGroundControl.Vehicle               1.0
import QGroundControl.QGCPositionManager    1.0
import QGroundControl.Airspace              1.0




Item{
    property real   _indicatorsHeight:      ScreenTools.defaultFontPixelHeight
    property real   _heading:               activeVehicle   ? activeVehicle.heading.rawValue : 0
    anchors.horizontalCenter: parent.horizontalCenter


    //-------------------------------------------------------------------------
    //-- Heading Indicator
    Rectangle {
        id:             compassBar
        height:         Screen.height * 0.08
        color:          "#00FFFFFF"
        width:          Screen.width * 0.4
        radius:         10
        clip:           true
        anchors.top:    parent.top
        anchors.topMargin: 20
        anchors.horizontalCenter: parent.horizontalCenter

        Repeater {
            model: 720
            anchors.centerIn: compassBar
            QGCLabel {
                function _normalize(degrees) {
                    var a = degrees % 360
                    if (a < 0) a += 360
                    return a
                }
                property int _startAngle: modelData + 180 + _heading
                property int _angle: _normalize(_startAngle)
                anchors.top: compassBar.top
                x:              visible ? ((modelData * (compassBar.width / 360)) - (width * 0.5)) : 0
                visible:        _angle % 45 == 0
                color:          _angle % 90 === 0 ? "white" : "grey"
                font.pointSize: 20
                font.bold:      _angle % 90 === 0 ? true : false
            text: {
                    switch(_angle) {
                    case 0:     return "|"
                    case 45:    return "ı"
                    case 90:    return "|"
                    case 135:   return "ı"
                    case 180:   return "|"
                    case 225:   return "ı"
                    case 270:   return "|"
                    case 315:   return "ı"
                    }
                    return ""
                }
            }
        }


        Repeater {
            model: 720
            anchors.centerIn: compassBar
            QGCLabel {
                function _normalize(degrees) {
                    var a = degrees % 360
                    if (a < 0) a += 360
                    return a
                }
                property int _startAngle: modelData + 180 + _heading
                property int _angle: _normalize(_startAngle)
                anchors.bottom: compassBar.bottom
                x:              visible ? ((modelData * (compassBar.width / 360)) - (width * 0.5)) : 0
                visible:        _angle % 45 == 0
                color:          "white"
                font.pointSize: ScreenTools.smallFontPointSize * 2
                font.bold:      true
            text: {
                    switch(_angle) {
                    case 0:     return "N"
                    case 45:    return "45"
                    case 90:    return "E"
                    case 135:   return "135"
                    case 180:   return "S"
                    case 225:   return "225"
                    case 270:   return "W"
                    case 315:   return "315"
                    }
                    return ""
                }
            }
        }

    }

    Rectangle {
        id:                         headingIndicator
        height:                     ScreenTools.defaultFontPixelHeight
        width:                      ScreenTools.defaultFontPixelWidth * 4
        color:                      qgcPal.windowShadeDark
        anchors.bottom:             compassBar.top
        anchors.bottomMargin:       ScreenTools.defaultFontPixelHeight * -0.1
        anchors.horizontalCenter:   parent.horizontalCenter
        QGCLabel {
            text:                   _heading
            color:                  qgcPal.text
            font.pointSize:         ScreenTools.smallFontPointSize + 5
            font.bold:              true
            anchors.centerIn:       parent
        }
    }
    Image {
        height:                     _indicatorsHeight * 2
        width:                      height
        source:                     "/custom/res/Images/compass_pointer.svg"
        fillMode:                   Image.PreserveAspectFit
        sourceSize.height:          height
        anchors.top:                compassBar.bottom
        anchors.topMargin:          ScreenTools.defaultFontPixelHeight * -0.5
        anchors.horizontalCenter:   parent.horizontalCenter
    }
}
