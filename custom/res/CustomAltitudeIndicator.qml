import QtQuick                  2.11
import QtQuick.Controls         2.4
import QtQuick.Layouts          1.11
import QtQuick.Dialogs          1.3
import QtPositioning            5.2
import QtQuick.Window           2.11

import QGroundControl                       1.0
import QGroundControl.Controllers           1.0
import QGroundControl.Controls              1.0
import QGroundControl.FlightMap             1.0
import QGroundControl.MultiVehicleManager   1.0
import QGroundControl.Palette               1.0
import QGroundControl.ScreenTools           1.0
import QGroundControl.Vehicle               1.0

import CustomAltitudeAdding                 1.0

Item {
    property var    _activeVehicle: QGroundControl.multiVehicleManager.activeVehicle ? QGroundControl.multiVehicleManager.activeVehicle : QGroundControl.multiVehicleManager.offlineEditingVehicle
    property real   _altitude: _activeVehicle ? - _activeVehicle.altitudeRelative.value + CustomAltitudeAdding.altitudeAddingValue : 0
    anchors.left: parent.left
    anchors.verticalCenter: parent.verticalCenter
    property int _model: 50


    Timer{
        interval: 500; running: true; repeat: true;
        onTriggered: {
              if(_model - _altitude < 50){
                  _model += 50 - (_model - _altitude)
                  console.log("Model: ", _model)
              }
        }
    }


    Rectangle{
        id: rect
        height: Screen.height * 0.4
        width: 150
        color: "#00ffffff"
        clip: true
        anchors.verticalCenter: parent.verticalCenter
        anchors.bottomMargin: 50
        Rectangle{
            anchors.verticalCenter: rect.verticalCenter
            color: "red"
//            y: parent.height / 2
            anchors.left: pointer.right
            Repeater{
                model: _model
                Column {
                    y: (modelData - _altitude) * 10 - lbl.height
                    visible: modelData % 2 === 0
                    QGCLabel{
                        id:   lbl
                        text: modelData % 10 === 0 ? "   " + modelData: " "
                        font.pointSize:    15
                    }
                    Rectangle{
                        color: "white"
                        height: 3
                        width: modelData % 10 === 0 ? 30 : 10
                    }
                }
            }
        }

        Rectangle{
            id: pointer
            anchors.left: parent.left
            anchors.verticalCenter: parent.verticalCenter
            height: 10
            width: 50
            color: "#ffffffff"
            radius: 30
//            Image {
//                anchors.verticalCenter: parent.verticalCenter
//                width: 40
//                height: 40
//                anchors.centerIn: parent
//                source: "/custom/res/Images/white-arrow.png"
//            }
        }
    }


}
