/****************************************************************************
 *
 * (c) 2009-2020 QGROUNDCONTROL PROJECT <http://www.qgroundcontrol.org>
 *
 * QGroundControl is licensed according to the terms in the file
 * COPYING.md in the root of the source code directory.
 *
 ****************************************************************************/


import QtQuick 2.3

import QGroundControl               1.0
import QGroundControl.Controls      1.0
import QGroundControl.ScreenTools   1.0
import QGroundControl.FactSystem    1.0
import QGroundControl.FlightMap     1.0
import QGroundControl.Palette       1.0

Rectangle {
    id:             root
    width:          getPreferredInstrumentWidth()
    height:         _outerRadius + _valuesWidget.height
    radius:         10
    color:          getTransparentColor()
    border.width:   1
    border.color:   _isSatellite ? qgcPal.mapWidgetBorderLight : qgcPal.mapWidgetBorderDark
    anchors.topMargin: 20

    property real   _innerRadius:       (width - (_topBottomMargin * 2)) / 2 //114
    property real   _outerRadius:       _innerRadius + _topBottomMargin * 2 //126
    property real   _defaultSize:       ScreenTools.defaultFontPixelHeight * (9)
    property real   _sizeRatio:         ScreenTools.isTinyScreen ? (width / _defaultSize) * 0.5 : width / _defaultSize
    property real   _bigFontSize:       ScreenTools.defaultFontPointSize * 2.5  * _sizeRatio
    property real   _normalFontSize:    ScreenTools.defaultFontPointSize * 1.5  * _sizeRatio
    property real   _labelFontSize:     ScreenTools.defaultFontPointSize * 0.75 * _sizeRatio
    property real   _spacing:           ScreenTools.defaultFontPixelHeight * 0.33
    property real   _topBottomMargin:   (width * 0.05) / 2 // 6
    property real   _availableValueHeight: maxHeight - attitudeAndCompass.height - 50

    function getTransparentColor(){
        var color = "#66" +  qgcPal.window.toString().replace("#","")
        console.log(color)
        return color
    }

    // Prevent all clicks from going through to lower layers
    DeadMouseArea {
        anchors.fill: parent
    }

    QGCPalette { id: qgcPal }


    Row{
        id:attitudeAndCompass
        height: _innerRadius
        QGCAttitudeWidget {
            id:                   attitude
            size:                 _innerRadius
            vehicle:              activeVehicle
        }
        QGCCompassWidget {
            id:                         compass
            size:                       _innerRadius
            vehicle:                    activeVehicle
        }
    }



    Item {
        id:                 _valuesItem
        anchors.topMargin:  ScreenTools.defaultFontPixelHeight / 4
        anchors.bottomMargin:  250/*ScreenTools.defaultFontPixelHeight / 4*/
        anchors.top:        attitudeAndCompass.bottom
        width:              parent.width
        visible:            widgetRoot.showValues

        // Prevent all clicks from going through to lower layers
        DeadMouseArea {
            anchors.fill: parent
        }

        Rectangle {
            anchors.fill:   _valuesWidget
            color:          getTransparentColor()
        }

        PageView {
            id:                 _valuesWidget
            anchors.margins:    1
            anchors.left:       parent.left
            anchors.right:      parent.right
            maxHeight:          _availableValueHeight
        }
    }


}
