
#pragma once
#include <QObject>

class CustomAltitudeAdding : public QObject
{
    Q_OBJECT
public:    
    CustomAltitudeAdding(QObject *parent = nullptr);
    ~CustomAltitudeAdding();
    Q_PROPERTY(int altitudeAddingValue READ altitudeAddingValue WRITE setAltitudeAddingValue NOTIFY altitudeAddingValueChanged)


    int altitudeAddingValue() {return _altitudeAddingValue;};
    void setAltitudeAddingValue (int value);

public slots:
    void incrementAltitudeAddingValue     (int incrementValue);
    void decrementAltitudeAddingValue     (int decrementValue);

signals:
    void altitudeAddingValueChanged();

private:
    int _altitudeAddingValue;


};
