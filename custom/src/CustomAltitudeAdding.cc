
#include "QGCApplication.h"

#include "CustomPlugin.h"
#include "CustomAltitudeAdding.h"

CustomAltitudeAdding::CustomAltitudeAdding(QObject *parent) : QObject(parent)
{
    _altitudeAddingValue = 0;
}

CustomAltitudeAdding::~CustomAltitudeAdding(){

}

void
CustomAltitudeAdding::setAltitudeAddingValue(int value){
    _altitudeAddingValue = value;
    emit altitudeAddingValueChanged();
}

void
CustomAltitudeAdding::incrementAltitudeAddingValue(int incrementValue){
    _altitudeAddingValue += incrementValue;
    emit altitudeAddingValueChanged();

}


void
CustomAltitudeAdding::decrementAltitudeAddingValue(int decrementValue){
    if(_altitudeAddingValue >= decrementValue){
        _altitudeAddingValue -= decrementValue;
    }else{
        _altitudeAddingValue = 0;
    }
    emit altitudeAddingValueChanged();
}


