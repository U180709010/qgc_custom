/****************************************************************************
 *
 * (c) 2009-2019 QGROUNDCONTROL PROJECT <http://www.qgroundcontrol.org>
 *
 * QGroundControl is licensed according to the terms in the file
 * COPYING.md in the root of the source code directory.
 *
 *   @brief Custom QGCCorePlugin Declaration
 *   @author Gus Grubba <gus@auterion.com>
 */

#pragma once

#include "QGCCorePlugin.h"
#include "QGCOptions.h"
#include "QGCLoggingCategory.h"
#include "VideoReceiver.h"
#include "SettingsManager.h"

#include <QTranslator>
#include <QStringList>

class CustomPlugin;
class CustomOptions;

Q_DECLARE_LOGGING_CATEGORY(CustomLog)



//-- Our own, custom options
class CustomOptions : public QGCOptions
{
public:
    CustomOptions(CustomPlugin*, QObject* parent = nullptr);
       //-- We have our own toolbar
    QUrl                    mainToolbarUrl                  () const final { return QUrl::fromUserInput("qrc:/custom/res/CustomMainToolBar.qml"); }
    QUrl                    flyViewOverlay                  () const final { return QUrl::fromUserInput("qrc:/custom/res/CustomFlightDisplayViewWidgets.qml"); }
    CustomInstrumentWidget* instrumentWidget                () final { return nullptr; }
};


//-----------------------------------------------------------------------------
class CustomPlugin : public QGCCorePlugin
{
    Q_OBJECT
public:
    CustomPlugin(QGCApplication* app, QGCToolbox *toolbox);
    ~CustomPlugin();

    QVariantList&           settingsPages                   () final;
    //QVariantList&           instrumentPages                 () final;
    void                    valuesWidgetDefaultSettings     (QStringList& largeValues, QStringList& smallValues) final;
    QString                 brandImageIndoor                () const final;
    QString                 brandImageOutdoor               () const final;
    QGCOptions*             options                         () final;
    QQmlApplicationEngine*  createRootWindow                (QObject* parent) final;
    void                    paletteOverride                 (QString colorName, QGCPalette::PaletteColorInfo_t& colorInfo) final;
    void                    setToolbox                      (QGCToolbox* toolbox);

private:
    void
    addSettingsEntry(
        const QString& title,
        const char* qmlFile,
        const char* iconFile = nullptr);
    //void
//    addInstrumentPageEntry(const QString& title,
//                                         const char* qmlFile);
private:
    CustomOptions*      _pOptions = nullptr;
    QVariantList        _customSettingsList;
    //typedef QGCCorePlugin _inherited;
};
